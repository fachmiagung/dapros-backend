const express = require('express');
const mysql = require('mysql2/promise');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const app = express();
const port = 3000;
const secretKey = 'your_secret_key'; // Replace with your secret key

// Create a connection pool using the promise-compatible version
const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'crud_app',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

// Middleware to log activities
function logActivity(username, activity) {
  const query = `
    INSERT INTO logs (username, activity)
    VALUES (?, ?)
  `;

  pool.query(query, [username, activity])
    .catch(error => {
      console.error(error);
    });
}

// Middleware to verify JWT token
async function verifyToken(req, res, next) {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ message: 'Authentication required' });
  }

  try {
    const decoded = jwt.verify(token, secretKey);
    req.user = decoded;
    logActivity(decoded.username, `Accessed ${req.method} ${req.path}`);
    next();
  } catch (error) {
    return res.status(403).json({ message: 'Token verification failed' });
  }
}

app.use(express.json());

// Register a user
app.post('/register', async (req, res) => {
  const { username, password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 10);

  const query = `
    INSERT INTO users (username, password)
    VALUES (?, ?)
  `;

  try {
    await pool.query(query, [username, hashedPassword]);
    res.json({ message: 'User registered successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Login and get JWT token
app.post('/login', async (req, res) => {
  const { username, password } = req.body;

  const query = `
    SELECT * FROM users WHERE username = ?
  `;

  try {
    const [results] = await pool.query(query, [username]);

    if (results.length === 0) {
      return res.status(401).json({ message: 'Authentication failed' });
    }

    const user = results[0];
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(401).json({ message: 'Authentication failed' });
    }

    const token = jwt.sign({ username: user.username }, secretKey, { expiresIn: '1h' });
    res.json({ token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Create a lead
app.post('/leads', verifyToken, async (req, res) => {
  const lead = req.body;
  const query = `
    INSERT INTO leads (business_name, website, contact_name, phone1, phone2, email_address, full_address, category, latitude, longitude)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;

  try {
    await pool.query(query, [
      lead.business_name,
      lead.website || null,
      lead.contact_name || null,
      lead.phone1 || null,
      lead.phone2 || null,
      lead.email_address || null,
      lead.full_address || null,
      lead.category || null,
      lead.latitude || null,
      lead.longitude || null
    ]);

    const activity = `Lead created: ${lead.business_name}`;
    logActivity(req.user.username, activity);

    res.json({ message: 'Lead created successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Read all leads
app.get('/leads', verifyToken, async (req, res) => {
  const query = `
    SELECT * FROM leads
  `;

  try {
    const [results] = await pool.query(query);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Update a lead
app.put('/leads/:id', verifyToken, async (req, res) => {
  const leadId = req.params.id;
  const updatedLead = req.body;
  const query = `
    UPDATE leads
    SET
      business_name = ?,
      website = ?,
      contact_name = ?,
      phone1 = ?,
      phone2 = ?,
      email_address = ?,
      full_address = ?,
      category = ?,
      latitude = ?,
      longitude = ?
    WHERE id = ?
  `;

  const existingLeadQuery = `SELECT * FROM leads WHERE id = ?`;

  try {
    const [existingLeadRows] = await pool.query(existingLeadQuery, [leadId]);
    const existingLead = existingLeadRows[0];

    const values = [
      updatedLead.business_name || existingLead.business_name,
      updatedLead.website || existingLead.website,
      updatedLead.contact_name || existingLead.contact_name,
      updatedLead.phone1 || existingLead.phone1,
      updatedLead.phone2 !== undefined ? updatedLead.phone2 : existingLead.phone2,
      updatedLead.email_address || existingLead.email_address,
      updatedLead.full_address || existingLead.full_address,
      updatedLead.category || existingLead.category,
      updatedLead.latitude || existingLead.latitude,
      updatedLead.longitude || existingLead.longitude,
      leadId
    ];

    await pool.query(query, values);

    const activity = `Lead updated: ${existingLead.business_name}`;
    logActivity(req.user.username, activity);

    res.json({ message: 'Lead updated successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Delete a lead
app.delete('/leads/:id', verifyToken, async (req, res) => {
  const leadId = req.params.id;
  const query = `DELETE FROM leads WHERE id = ?`;

  const existingLeadQuery = `SELECT * FROM leads WHERE id = ?`;

  try {
    const [existingLeadRows] = await pool.query(existingLeadQuery, [leadId]);
    const existingLead = existingLeadRows[0];

    await pool.query(query, [leadId]);

    const activity = `Lead deleted: ${existingLead.business_name}`;
    logActivity(req.user.username, activity);

    res.json({ message: 'Lead deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Read a specific lead by ID
app.get('/leads/:id', verifyToken, async (req, res) => {
  const leadId = req.params.id;
  const query = `
    SELECT * FROM leads WHERE id = ?
  `;

  try {
    const [results] = await pool.query(query, [leadId]);

    if (results.length === 0) {
      return res.status(404).json({ message: 'Lead not found' });
    }

    res.json(results[0]);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Read lead history
app.get('/leads/:id/history', verifyToken, async (req, res) => {
  const leadId = req.params.id;
  const query = `
    SELECT * FROM lead_history WHERE lead_id = ? ORDER BY timestamp DESC
  `;

  try {
    const [results] = await pool.query(query, [leadId]);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred' });
  }
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
